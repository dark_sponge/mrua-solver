# coding: utf-8
from math import *  # Importation racine carée
import tkinter as tk  # Importation de tkinter pour pouvoir créer une interface graphique
from PIL import ImageTk, Image  # Importation de PIL pour gérer les images
import tkinter.messagebox  # Importation de messagebox pour les pop-ups.

window = tk.Tk()  # Création d'une fenêtre avec tkinter
window.title('Uniformly accelerated motion calculator')  # Nom de la fenêtre et du programme
window.geometry('850x300')  # Dimensions de la fenêtre
window.configure(bg="#18191c") # Couleur de l'arrière-plan
window.bind('<KeyRelease-Return>', lambda i : PressEnter(i))
window.iconbitmap('calculator.ico')

tour = 0 # Est incrémenté de 1 à chaque nouvelle résolution

arrow = Image.open("arrow.png")  # Importation de l'image de la flèche
arrow = ImageTk.PhotoImage(arrow)  # Création de la variable contenant la flèche
ARROW = tk.Label(window, image=arrow)  # Création de la variable tkinter contenant la flèche
ARROW.configure(bg="#18191c") # Couleur d'arrière-plan de la flèche

# Création des cinq variables nécessaires à un MRUA
V0 = None  # Vitesse initiale
V1 = None  # Vitesse finale
A = None  # Accélération
D = None  # Distance
T = None  # Temps

# Unités par défaut des variables
UnitV0 = "m/s"
UnitV1 = "m/s"
UnitA = "m/s2"
UnitD = "m"
UnitT = "s"

# Création des Entry pour les variables
inputV0 = tk.Entry(window, show=None, font=('Arial', 14), width=18)
inputV1 = tk.Entry(window, show=None, font=('Arial', 14), width=18)
inputA = tk.Entry(window, show=None, font=('Arial', 14), width=18)
inputD = tk.Entry(window, show=None, font=('Arial', 14), width=18)
inputT = tk.Entry(window, show=None, font=('Arial', 14), width=18)

# Apparition des Entry
inputV0.grid(row=0, column=1, sticky="we", pady=(10, 0), padx=(0, 5))
inputV1.grid(row=1, column=1, sticky="we", padx=(0, 5))
inputA.grid(row=2, column=1, sticky="we", padx=(0, 5))
inputD.grid(row=3, column=1, sticky="we", padx=(0, 5))
inputT.grid(row=4, column=1, sticky="we", padx=(0, 5))

# Création des Labels pour les Entry
lblV0 = tk.Label(window, text="Initial Speed", bd=3, bg="#36393f", fg="#b9bbbe")
lblV1 = tk.Label(window, text="Final Speed", bd=3, bg="#36393f", fg="#b9bbbe")
lblA = tk.Label(window, text="Acceleration", bd=3, bg="#36393f", fg="#b9bbbe")
lblD = tk.Label(window, text="Distance", bd=3, bg="#36393f", fg="#b9bbbe")
lblT = tk.Label(window, text="Time", bd=3, bg="#36393f", fg="#b9bbbe")

# Apparition des Labels
lblV0.grid(row=0, column=0, sticky="we", padx=(20, 5), pady=(10, 0))
lblV1.grid(row=1, column=0, sticky="we", padx=(20, 5))
lblA.grid(row=2, column=0, sticky="we", padx=(20, 5))
lblD.grid(row=3, column=0, sticky="we", padx=(20, 5))
lblT.grid(row=4, column=0, sticky="we", padx=(20, 5))

# Création des Labels des résultats
police = ('Arial', 17)
otpV0 = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpV1 = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpA = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpD = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpT = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)

# Création des Labels contenant les unités des résultats
otpV0unit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpV1unit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpAunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpDunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpTunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)

otpLabelV0 = tk.Label(window, bd=3, text='V0 = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelV1 = tk.Label(window, bd=3, text='V1 = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelA = tk.Label(window, bd=3, text='A = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelD = tk.Label(window, bd=3, text='D = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelT = tk.Label(window, bd=3, text='T = ', bg="#18191c", fg="#b9bbbe", font=police)

bgclearBtn = "#ce232b" # On indique ici les couleurs utilisée pour les boutons clear
abgclearBtn = "#fc647c"
hlclearbutton = "#f6052d"
clearBtnV0 = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=2, highlightcolor=hlclearbutton)
clearBtnV1 = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=2, highlightcolor=hlclearbutton)
clearBtnA = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=2, highlightcolor=hlclearbutton)
clearBtnD = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=2, highlightcolor=hlclearbutton)
clearBtnT = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=2, highlightcolor=hlclearbutton)


class Clearmanagement: # classe utilisée pour créer les boutons clear
    def __init__(self, inputX):
        self.inputX = inputX

    def clearing(self):
        self.inputX.delete(0, 'end')


rowV0 = Clearmanagement(inputV0)
rowV1 = Clearmanagement(inputV1)
rowA = Clearmanagement(inputA)
rowD = Clearmanagement(inputD)
rowT = Clearmanagement(inputT)

clearBtnV0.config(command=rowV0.clearing, highlightbackground="#18191c")
clearBtnV0.grid(row=0, column=4, sticky="we", padx=(2, 5), pady=(10, 0))
clearBtnV1.config(command=rowV1.clearing, highlightbackground="#18191c")
clearBtnV1.grid(row=1, column=4, sticky="we", padx=(2, 5))
clearBtnA.config(command=rowA.clearing, highlightbackground="#18191c")
clearBtnA.grid(row=2, column=4, sticky="we", padx=(2, 5))
clearBtnD.config(command=rowD.clearing, highlightbackground="#18191c")
clearBtnD.grid(row=3, column=4, sticky="we", padx=(2, 5))
clearBtnT.config(command=rowT.clearing, highlightbackground="#18191c")
clearBtnT.grid(row=4, column=4, sticky="we", padx=(2, 5))

Ltofind = []  # Contient les variables à trouver en string, par exemple V0 -> "V0"
Lresult = []  # Contient les tout les résultats

def ScientificNotation(var): # Cette fonction s'occupe de transformer une valeur en notation scientifique, si cela est nécessaire.
    global scn
    scn = False # Vaut True si une valeur a été transformée en notation scientifique
    strVar = str(var)
    print(strVar)
    #print("strVar: ", strVar)
    lenint = 0
    lendec = 0
    comma = False

    if "e" in strVar: #Si la variable contient un "e", c'est qu'elle est déja en notation scientifique, ce qui peut arriver car python le fait parfois automatiquement
        scn = True
        return "{:.2e}".format(var)
    else:
        for i in strVar: # Sinon on parcoure la variable en contant le nombre de chiffre et de zéros après la virgule
            if i == "." and i != "-":
                comma = True
            elif comma == False:
                lenint += 1
            if comma == True and i == "0":
                lendec += 1

        # Si l'échelle est assez grande ou assez petite, on convertit
        if lenint > 5:
            scn = True
            return "{:.2e}".format(var)
        elif lendec > 3:
            scn = True
            return "{:.2e}".format(var)
        else:
            return var

def changedecimales(a): # Fonction s'activant lors d'un changement du nombre de décimales, permettant de changer l'affichage des résultats de manière rétroactive.
    global decimales
    decimales = int(tkvarDec.get()) # On va chercher la valeur du menu déroulant
    print(decimales)
    OutputResult()

def PressEnter(key): # Fonction s'activant quand la touche "Return" est pressée.
    x = key.char
    presubmit()

def presubmit(): # Cette fonction vérifie que les valeurs sont compatibles entre elles, et que les lacunes ne contiennent pas de caractères non-authorisés
    global V0, V1, A, D, T
    global UnitV0, UnitV1, UnitA, UnitD, UnitT, Ltofind, Ltofindold, Numberofvariables, decimales, UnitV0old, UnitV1old, UnitAold, UnitDold, UnitTold
    NoError = True
    Numberofvariables = 0

    Ltofindold = Ltofind # Afin de restaurer les précédents résultats s'il y en a, on les stocke dans une liste

    UnitV0old = UnitV0 # Pareil pour les unités
    UnitV1old = UnitV1
    UnitAold = UnitA
    UnitDold = UnitD
    UnitTold = UnitT

    Ltofind = [] # Une fois qu'on les a sauvegardés, on vide Ltofind, pour ne pas perturber le nouveau tour

    # Récupératio du contenu des Entry
    V0 = inputV0.get()
    V1 = inputV1.get()
    A = inputA.get()
    if A == "g":
        A = 9.80665
    D = inputD.get()
    T = inputT.get()

    # Récupération des unités sélectionnées
    UnitV0 = tkvarV0.get()
    UnitV1 = tkvarV1.get()
    UnitA = tkvarA.get()
    UnitD = tkvarD.get()
    UnitT = tkvarT.get()

    # Définition du nombre de décimales maximum à cacluler
    decimales = int(tkvarDec.get())

    try:  # Le try sert ici à vérifier que les Entry ne contiennent que des chiffres

        # Vérification caractères spéciaux
        if V0 != '':  # On vérifie uniquement quand l'Entry n'est pas vide
            V0 = float(
                V0)  # Si python ne parvient pas à transformer la variable en float, c'est que V0 contient des caractères non-authorisés
            Numberofvariables += 1 # Si la lacune n'est pas vide, on incrémente Numberofvariables de 1, pour savoir le nombre de variables utilisées.
        elif V0 == '': # Si une lacune est vide ...
            V0 = None
            if "V0" not in Ltofind:
                Ltofind.append('V0')  # On met la variable dans la liste de variables à trouver

        if V1 != '':
            V1 = float(V1)
            Numberofvariables += 1
        elif V1 == '':
            V1 = None
            if "V1" not in Ltofind:
                Ltofind.append('V1')

        if A != '':
            A = float(A)
            Numberofvariables += 1
        elif A == '':
            A = None
            if "A" not in Ltofind:
                Ltofind.append('A')

        if D != '':
            D = float(D)
            Numberofvariables += 1
        elif D == '':
            D = None
            if "D" not in Ltofind:
                Ltofind.append('D')

        if T != '':
            T = float(T)
            Numberofvariables += 1
        elif T == '':
            T = None
            if "T" not in Ltofind:
                Ltofind.append('T')
        print("Ltofind: ", Ltofind)

    except:
        tk.messagebox.showinfo('Error',"Please check that the inputs only contains numbers")  # Message d'erreur
        NoError = False # Empêche la suite du programme de s'exécuter

    # Vérification des cas impossibles
    if "A" not in Ltofind and "V0" not in Ltofind and "V1" not in Ltofind:
        if A < 0 and V1 > V0:
            tk.messagebox.showinfo('Error', "Final speed cannot be bigger than Initial speed with a decelaration.")
            NoError = False

        if A > 0 and V0 > V1:
            tk.messagebox.showinfo('Error', "Initial speed cannot be bigger than Final speed with an acceleration")
            NoError = False

    if "T" not in Ltofind:
        if T <= 0:
            tk.messagebox.showinfo('Error', "Time cannot be negative nor null")
            NoError = False


    if Numberofvariables < 3:
        tk.messagebox.showinfo('Error', "Please input at least 3 variables")
        NoError = False

    if NoError == True: # Si on a aucune erreur, on lance la fonctin suivante.
        submit()


def submit():  # Fonction s'exécutant si aucune erreur n'est apparue dans presubmit()
    # On rend ici les variables contenant les unités globale, pour pouvoir y accéder depuis partout.
    global UnitV0, UnitV1, UnitA, UnitD, UnitT
    global V0, V1, A, D, T

    # Disparition des résultats précédents, s'il y en a
    otpV0.grid_forget()
    otpV1.grid_forget()
    otpA.grid_forget()
    otpD.grid_forget()
    otpT.grid_forget()

    ARROW.grid_forget()  # Disparion de la flèche

    # Disparition des unités des résultats précédents
    otpV0unit.grid_forget()
    otpV1unit.grid_forget()
    otpAunit.grid_forget()
    otpDunit.grid_forget()
    otpTunit.grid_forget()

    # Disparition des Labels des résultats précédents
    otpLabelV0.grid_forget()
    otpLabelV1.grid_forget()
    otpLabelA.grid_forget()
    otpLabelD.grid_forget()
    otpLabelT.grid_forget()

    # Transformation des unités dans le système international
    if UnitV0 == "cm/s" and V0 != None:
        V0 = V0 / 100
    elif UnitV0 == "km/s" and V0 != None:
        V0 = V0 * 1000
    elif UnitV0 == "km/h" and V0 != None:
        V0 = V0 / 3.6

    if UnitV1 == "cm/s" and V1 != None:
        V1 = V1 / 100
    elif UnitV1 == "km/s" and V1 != None:
        V1 = V1 * 1000
    elif UnitV1 == "km/h" and V1 != None:
        V1 = V1 / 3.6

    if UnitD == "mm" and D != None:
        D = D / 1000
    elif UnitD == "cm" and D != None:
        D = D / 100
    elif UnitD == "km" and D != None:
        D = D * 1000

    if UnitT == "ms" and T != None:
        T = T / 1000

    resolveurEquation(V0, V1, A, D, T)  # Résolution du système d'équation


def resolveurEquation(V0, V1, A, D, T):  # Fonction responsable de résoudre le MRUA
    global Lresult, UnitV0, UnitV1, UnitA, UnitD, UnitT, Numberofvariables, Lresultold, mistake

    NoError2 = True # Vaut True si aucune erreur ne s'est produite. 

    if tour >= 1:
        Lresultold = Lresult # Stocke les résultats du tour précédent dans une variable

    # Résolution du système, différente en fonction des inconnues recherchées.
    if V0 is None:
        if V1 is None:
            V0 = (2 * D - A * T ** 2) / 2 * T
            V1 = V0 + A * T
        if A is None:
            A = 2 * (V1 * T - D) / T ** 2
            V0 = V1 - A * T
        if D is None:
            V0 = V1 - A * T
            D = 0.5 * A * T ** 2 + V0 * T
        if T is None:
            V0 = sqrt(V1 ** 2 - 2 * A * D)
            T = 2 * D / (V0 + V1)
        else:
            V0 = V1 - A * T

    if V1 is None:
        if A is None:
            V1 = 2 * D * T - V0
            A = (V1 - V0) / T
        if D is None:
            V1 = V0 + A * T
            D = 0.5 * A * T ** 2 + V0 * T
        if T is None:
            V1 = sqrt(V0 ** 2 + 2 * A * D)
            T = 2 * D / (V0 + V1)
        else:
            V1 = V0 + A * T

    if A is None:
        if D is None:
            A = (V1 - V0) / T
            D = 0.5 * A * T ** 2 + V0 * T
        if T is None:
            T = 2 * D / (V0 + V1)
            A = (V1 - V0) / T
        else:
            A = (V1 - V0) / T
    if D is None:
        if T is None:
            T = (V1 - V0) / A
            D = 0.5 * A * T ** 2 + V0 * T
        else:
            D = 0.5 * A * T ** 2 + V0 * T
    if T is None:
        T = (V1 - V0) / A

    if Numberofvariables == 4: # Dans le cas où 4 variables sont indiquées, on vérifie que le système n'est pas incohérent en recalculant chaque résultat de deux manières différentes en comparant les valeurs ainsi obtenues.
        mistake = False
        V0_1 = V1 - A * T
        V0_2 = (2 * D - A * T ** 2) / 2 * T
        if V0_1 != V0_2:
            mistake = True

        V1_1 = V0 + A * T
        V1_2 = 2 * D * T - V0
        if V1_1 != V1_2:
            mistake = True

        A_1 = 2 * (V1 * T - D) / T ** 2
        A_2 = (V1 - V0) / T
        if A_1 != A_2:
            mistake = True

        D_1 = 0.5 * A * T ** 2 + V0 * T
        D_2 = 0.5 * (V0 + V1) * T
        if D_1 != D_2:
            mistake = True

        T_1 = 2 * D / (V0 + V1)
        T_2 = (V1 - V0) / A
        if T_1 != T_2:
            mistake = True

        if mistake == True: # Si une incohérence apparaît, on affiche un message d'erreur.
            NoError2 = False
            tk.messagebox.showinfo('Error', "Not scientifically possible")

    if Numberofvariables == 5: # Si 5 variables sont indiquées, l'utilisateur n'a pas besoin du programme
        stupide = tk.messagebox.askyesno("Hmm","You dumb?")
        while stupide == False:
            stupide = tk.messagebox.askyesno("Hmm","You dumb?")
        NoError2 = False
    
    if NoError2 == True: # S'il n'y a pas d'incohérence, on passe à la suite.
       
        # Reconversion des résultats dans les unités sélectionnées par l'utilisateur
        if UnitV0 == "cm/s":
            V0 = V0 * 100
        elif UnitV0 == "km/s":
            V0 = V0 / 1000
        elif UnitV0 == "km/h":
            V0 = V0 * 3.6

        if UnitV1 == "cm/s":
            V1 = V1 * 100
        elif UnitV1 == "km/s":
            V1 = V1 / 1000
        elif UnitV1 == "km/h":
            V1 = V1 * 3.6

        if UnitD == "mm":
            D = D * 1000
        elif UnitD == "cm":
            D = D * 100
        elif UnitD == "km":
            D = D / 1000

        if UnitT == "ms":
            T = T * 1000

        Lresult = [V0, V1, A, D, T]  # Les résultats sont stockés dans Lresult
        ARROW.grid(row=0, column=5, rowspan=5, padx=40, pady=(10,0))  # Apparition de la flèche
        OutputResult()  # Rendu des résultats

def OutputResult():  # Fonction qui s'occupe de faire apparaître les résultats
    global Ltofind, Lresult, tour

    tour += 1

    V0 = ScientificNotation(Lresult[0]) # On transforme si nécessaire en notation scientifique
    if scn == False and "V0" in Ltofind: # Si la valeur n'est pas en notation scientifique, on l'arrondi au nombre de décimales souhaitées
        V0 = round(Lresult[0], decimales)

    V1 = ScientificNotation(Lresult[1])
    if scn == False and "V1" in Ltofind:
        V1 = round(Lresult[1], decimales)

    A = ScientificNotation(Lresult[2])
    if scn == False and "A" in Ltofind:
        A = round(Lresult[2], decimales)

    D = ScientificNotation(Lresult[3])
    if scn == False and "D" in Ltofind:
        D = round(Lresult[3], decimales)
        print("D scn", scn)

    T = ScientificNotation(Lresult[4])
    if scn == False and "T" in Ltofind:
        T = round(Lresult[4], decimales)

    Lresult = [V0,V1,A,D,T] # On remet les valeurs ainsi modifiées dans Lresult, pour pouvour les afficher correctement

    try:
        if V0 - int(V0) == 0:
            V0 = int(V0)  # Si on a un nombre entier, on le retransforme en int pour plus de lisibilité (on évite ainsi d'afficher 10.0).
    except:
        pass

    try:
        if V1 - int(V1) == 0:
            V1 = int(V1)
    except:
        pass

    try:
        if A - int(A) == 0:
            A = int(A)
    except:
        pass

    try:
        if D - int(D) == 0:
            D = int(D)
    except:
        pass

    try:
        if T - int(T) == 0:
            T = int(T)
    except:
        pass

    # Apparition résultat V0
    otpLabelV0.grid(row=0, column=6, pady=(10, 0), sticky="e")
    otpV0.config(text=V0)
    otpV0.grid(row=0, column=7, pady=(10, 0))
    otpV0unit.config(text=UnitV0)
    otpV0unit.grid(row=0, column=8, sticky="w", pady=(10, 0))

    # Apparition résultat V1
    otpLabelV1.grid(row=1, column=6, sticky="e")
    otpV1.config(text=V1)
    otpV1.grid(row=1, column=7)
    otpV1unit.config(text=UnitV1)
    otpV1unit.grid(row=1, column=8, sticky="w")

    # Apparition résultat A
    otpLabelA.grid(row=2, column=6, sticky="e")
    otpA.config(text=A)
    otpA.grid(row=2, column=7)
    otpAunit.config(text=UnitA)
    otpAunit.grid(row=2, column=8, sticky="w")

    # Apparition résultat D
    otpLabelD.grid(row=3, column=6, sticky="e")
    otpD.config(text=D)
    otpD.grid(row=3, column=7)
    otpDunit.config(text=UnitD)
    otpDunit.grid(row=3, column=8, sticky="w")

    # Apparition résultat T
    otpLabelT.grid(row=4, column=6, sticky="e")
    otpT.config(text=T)
    otpT.grid(row=4, column=7)
    otpTunit.config(text=UnitT)
    otpTunit.grid(row=4, column=8, sticky="w")

    if tour >= 2:
        bouton2.grid(column=1, row=7, sticky='we', padx=(0, 5))  # Apparition du bouton RESUME, qui permet de rétablir les précédents résultats


def reset(): # Remet le programme à zéro lorsque l'on presse le bouton CLEAR ALL
    global V0, V1, A, D, T, Ltofind, Ltofindold, Lresultold
    Ltofindold = Ltofind # On stocke Ltofind dans Ltofindold au cas ou l'utilisateur veut rétablir le valeur qu'il avait entrée avant de presser CLEAR ALL
    
    if tour != 0:
        Lresultold = Lresult
        bouton2.grid(column=1, row=7, sticky='we', padx=(0, 5))  # Apparition du bouton RESUME, qui n'est pas présent lorsqu'il n'y a rien à restaurer

    # Remise à zéro des variables
    V0 = None
    V1 = None
    A = None
    D = None
    T = None

    # Vidage des Entry
    inputV0.delete(0, 'end')
    inputV1.delete(0, 'end')
    inputA.delete(0, 'end')
    inputD.delete(0, 'end')
    inputT.delete(0, 'end')

    # Disparition des résultats
    otpV0.grid_forget()
    otpV1.grid_forget()
    otpA.grid_forget()
    otpD.grid_forget()
    otpT.grid_forget()

    # On remet les unités de base
    tkvarV0.set(UnitsV0[1])
    tkvarV1.set(UnitsV1[1])
    tkvarA.set(UnitsA[0])
    tkvarD.set(UnitsD[2])
    tkvarT.set(UnitsT[1])

    ARROW.grid_forget()  # Disparion de la flèche

    # Disparition des unités des résultats
    otpV0unit.grid_forget()
    otpV1unit.grid_forget()
    otpAunit.grid_forget()
    otpDunit.grid_forget()
    otpTunit.grid_forget()

    # Disparition des Labels des résultats
    otpLabelV0.grid_forget()
    otpLabelV1.grid_forget()
    otpLabelA.grid_forget()
    otpLabelD.grid_forget()
    otpLabelT.grid_forget()

def resume():  # Fonction qui s'occupe de faire réapparaître les anciens résultats
    global Ltofind, Lresult
    if tour == 0: # Si il n'y a pas de précédents resultats, on ne lance pas la restauration
        tk.messagebox.showinfo('Error', "No previous results available")
    else:
        # Apparition résultat V0 précédent
        otpV0.config(text=Lresultold[0])
        otpV0.grid(row=0, column=7, pady=(10, 0))
        otpV0unit.config(text=UnitV0old)
        otpV0unit.grid(row=0, column=8, sticky="w", pady=(10, 0))
        otpLabelV0.grid(row=0, column=6, pady=(10, 0), sticky="e")

        # Apparition résultat V1 précédent
        otpV1.config(text=Lresultold[1])
        otpV1.grid(row=1, column=7)
        otpV1unit.config(text=UnitV1old)
        otpV1unit.grid(row=1, column=8, sticky="w")
        otpLabelV1.grid(row=1, column=6, sticky="e")

        # Apparition résultat A précédent
        otpA.config(text=Lresultold[2])
        otpA.grid(row=2, column=7)
        otpAunit.config(text=UnitAold)
        otpAunit.grid(row=2, column=8, sticky="w")
        otpLabelA.grid(row=2, column=6, sticky="e")

        # Apparition résultat D
        otpD.config(text=Lresultold[3])
        otpD.grid(row=3, column=7)
        otpDunit.config(text=UnitDold)
        otpDunit.grid(row=3, column=8, sticky="w")
        otpLabelD.grid(row=3, column=6, sticky="e")

        # Apparition résultat T précédent
        otpT.config(text=Lresultold[4])
        otpT.grid(row=4, column=7)
        otpTunit.config(text=UnitTold)
        otpTunit.grid(row=4, column=8, sticky="w")
        otpLabelT.grid(row=4, column=6, sticky="e")

        # Vidage des Entry
        inputV0.delete(0, "end")
        inputV1.delete(0, "end")
        inputA.delete(0, "end")
        inputD.delete(0, "end")
        inputT.delete(0, "end")

        # On remet les valeurs qui étaient dans les input
        if "V0" not in Ltofindold:
            inputV0.insert(0, Lresultold[0])

        if "V1" not in Ltofindold:
            inputV1.insert(0, Lresultold[1])

        if "A" not in Ltofindold:
            inputA.insert(0, Lresultold[2])

        if "D" not in Ltofindold:
            inputD.insert(0, Lresultold[3])

        if "T" not in Ltofindold:
            inputT.insert(0, Lresultold[4])

        bouton2.grid_forget() #On enlève le bouton RESUME, car il n'est possible de restaurer qu'une seule fois

# Création des menus dropdown pour les unités
ddactivebg = "#36393f"

UnitsV0 = ["cm/s", "m/s", "km/s", "km/h"]  # On met toutes les possibilités dans une liste
tkvarV0 = tk.StringVar(window)  # On crée la variable qui contient le choix sélectionné
tkvarV0.set(UnitsV0[1])  # On choisit la valeur d'unité par défaut
UnitV0Menu = tk.OptionMenu(window, tkvarV0, *UnitsV0)  # On crée le menu dropdown
UnitV0Menu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238") # Réglages cosmétiques
UnitV0Menu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitV0Menu.grid(row=0, column=2, sticky="we", pady=(10, 0))

UnitsV1 = ["cm/s", "m/s", "km/s", "km/h"]
tkvarV1 = tk.StringVar(window)
tkvarV1.set(UnitsV1[1])
UnitV1Menu = tk.OptionMenu(window, tkvarV1, *UnitsV1)
UnitV1Menu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitV1Menu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitV1Menu.grid(row=1, column=2, sticky="we")

UnitsA = ["m/s2"]
tkvarA = tk.StringVar(window)
tkvarA.set(UnitsA[0])
UnitAMenu = tk.OptionMenu(window, tkvarA, *UnitsA)
UnitAMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitAMenu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitAMenu.grid(row=2, column=2, sticky="we")

UnitsD = ["mm", "cm", "m", "km"]
tkvarD = tk.StringVar(window)
tkvarD.set(UnitsD[2])
UnitDMenu = tk.OptionMenu(window, tkvarD, *UnitsD)
UnitDMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitDMenu["menu"].config(bg=ddactivebg, activebackground="#303238")
UnitDMenu.grid(row=3, column=2, sticky="we")

UnitsT = ["ms", "s"]
tkvarT = tk.StringVar(window)
tkvarT.set(UnitsT[1])
UnitTMenu = tk.OptionMenu(window, tkvarT, *UnitsT)
UnitTMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitTMenu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitTMenu.grid(row=4, column=2, sticky="we")

decimalespossibles = [2,3,4,5,6,7,8,9]
tkvarDec = tk.StringVar(window)
tkvarDec.set(decimalespossibles[0]) # Menu dropdown pour le nombre de déciales

UnitDecMenu = tk.OptionMenu(window, tkvarDec, *decimalespossibles, command=changedecimales)
UnitDecMenu.configure(highlightbackground="#18191c", width=4)
UnitDecMenu.grid(row=6, column=5, padx=40)

lblDec = tk.Label(window, text="Decimals")
lblDec.config(bd=3, bg="#36393f", fg="#b9bbbe")
lblDec.grid(row=5, column=5, padx=40, pady=(5,5))

bouton = tk.Button(text="SUBMIT", command=presubmit, bg="#36393f", fg="#b9bbbe", highlightbackground="#18191c")  
bouton.grid(column=1, row=5, sticky='we', padx=(0,5))  # Apparition du bouton SUBMIT

bouton1 = tk.Button(text="CLEAR ALL", command=reset, bg="#36393f", fg="#b9bbbe", highlightbackground="#18191c") 
bouton1.grid(column=1, row=6, sticky='we', padx=(0,5), pady=(0,2))   # Apparition du bouton RESET

bouton2 = tk.Button(text="RESUME LAST RESULTS", command=resume, bg="#36393f", fg="#b9bbbe", highlightbackground="#18191c")

window.mainloop()  # Apparition de la fenêtre